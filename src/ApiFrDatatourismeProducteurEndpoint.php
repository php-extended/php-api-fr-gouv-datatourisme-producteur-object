<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-producteur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeProducteur;

use InvalidArgumentException;
use Iterator;
use LogicException;
use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use PhpExtended\Slugifier\SlugifierFactory;
use PhpExtended\Slugifier\SlugifierFactoryInterface;
use PhpExtended\Slugifier\SlugifierInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;

/**
 * ApiFrDatatourismeProducteurEndpoint class file.
 * 
 * This class is a simple implementation of the ApiFrDatatourismeProducteurEndpointInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiFrDatatourismeProducteurEndpoint implements ApiFrDatatourismeProducteurEndpointInterface
{
	
	public const HOST = 'https://producteur.datatourisme.fr/';
	public const EXPORT_COPY_TIMEOUT = 600;
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	
	/**
	 * The slugifier factory.
	 * 
	 * @var SlugifierFactoryInterface
	 */
	protected SlugifierFactoryInterface $_slugifierFactory;
	
	/**
	 * The slugifier.
	 * 
	 * @var ?SlugifierInterface
	 */
	protected ?SlugifierInterface $_slugifier = null;
	
	/**
	 * The directory where to store the files.
	 * 
	 * @var string
	 */
	protected string $_storeDirectory;
	
	/**
	 * Whether the endpoint is logged in.
	 * 
	 * @var boolean
	 */
	protected bool $_loggedIn = false;
	
	/**
	 * Builds a new ApiFrDatatourismeProducteurEndpoint with the given http endpoint. The http
	 * client should support cookies for this endpoint to work.
	 * 
	 * @param string $tempDirectoryPath
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 * @param ?SlugifierFactoryInterface $slugifierFactory
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	public function __construct(
		string $tempDirectoryPath,
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null,
		?SlugifierFactoryInterface $slugifierFactory = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		$this->_slugifierFactory = $slugifierFactory ?? new SlugifierFactory();
		
		$storeDir = \realpath($tempDirectoryPath);
		if(false === $storeDir)
		{
			// @codeCoverageIgnoreStart
			$message = 'The given export path is not a real directory at {path}';
			$context = ['{path}' => $storeDir];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		if(!\is_dir($storeDir))
		{
			// @codeCoverageIgnoreStart
			$message = 'The given export dir is not a directory at {dir}';
			$context = ['{dir}' => $storeDir];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		if(!\is_writable($storeDir))
		{
			// @codeCoverageIgnoreStart
			$message = 'The given export dir is not writeable at {dir}';
			$context = ['{dir}' => $storeDir];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		$this->_storeDirectory = $storeDir;
		
		$configuration = $this->_reifier->getConfiguration();
		$configuration->addFieldsAllowedToFail(ApiFrDatatourismeProducteurStatReutilisationLine::class, ['url', 'Url']);
		$configuration->addFieldNameAlias(ApiFrDatatourismeProducteurStatReutilisationLine::class, 'modeTelechargement', 'Mode téléchargement');
		$configuration->addFieldNameAlias(ApiFrDatatourismeProducteurStatReutilisationLine::class, 'dateTelechargement', 'Date téléchargement');
		$configuration->addFieldNameAlias(ApiFrDatatourismeProducteurStatReutilisationLine::class, 'createur', 'Créateur');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrDatatourismeProducteur\ApiFrDatatourismeProducteurEndpointInterface::login()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable should not happen
	 * @throws RuntimeException
	 */
	public function login(string $username, string $password) : bool
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'login');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$response = $this->_httpClient->sendRequest($request);
		$data = $response->getBody()->__toString();
		
		$matches = [];
		if(!\preg_match('#<input type="hidden" name="_csrf_token" value="([^"]+)">#', $data, $matches))
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to find csrf token value at "{url}".';
			$context = ['{url}' => $response->getHeaderLine('X-Target-Uri')];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		/** @phpstan-ignore-next-line */
		$csrfToken = $matches[1] ?? '';
		
		$query = \http_build_query([
			'_username' => $username,
			'_password' => $password,
			'_csrf_token' => $csrfToken,
		]);
		
		$request = $this->_requestFactory->createRequest('POST', $uri);
		$request = $request->withAddedHeader('Content-Type', 'application/x-www-form-urlencoded');
		$request = $request->withAddedHeader('Content-Length', (string) \mb_strlen($query));
		$request = $request->withAddedHeader('X-Php-Follow-Location', '0');
		$request = $request->withBody($this->_streamFactory->createStream($query));
		$response = $this->_httpClient->sendRequest($request);
		
		if(!$response->hasHeader('Location'))
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to login with username {username}, redirection to home page not found from url "{url}".';
			$context = [
				'{username}' => $username,
				'{url}' => $response->getHeaderLine('X-Target-Uri'),
			];
			
			throw new InvalidArgumentException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		return $this->_loggedIn = true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrDatatourismeProducteur\ApiFrDatatourismeProducteurEndpointInterface::copyTodaysReutilisationStatsFile()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws LogicException
	 * @throws ParseThrowable should not happen
	 * @throws RuntimeException
	 */
	public function copyTodaysReutilisationStatsFile(string $path) : bool
	{
		if(!$this->_loggedIn)
		{
			// @codeCoverageIgnoreStart
			throw new LogicException('You must be logged in before copying the reutilisation file.');
			// @codeCoverageIgnoreEnd
		}
		
		if(\mb_strlen($path) > 0 && '/' !== $path[0])
		{
			$path = $this->_storeDirectory.'/'.$path;
		}
		
		$exportDir = \realpath(\dirname($path));
		if(false === $exportDir)
		{
			// @codeCoverageIgnoreStart
			$message = 'The given export path is not a real directory at {path}';
			$context = ['{path}' => $exportDir];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		if(!\is_dir($exportDir))
		{
			// @codeCoverageIgnoreStart
			$message = 'The given export dir is not a directory at {dir}';
			$context = ['{dir}' => $exportDir];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		if(!\is_writable($exportDir))
		{
			// @codeCoverageIgnoreStart
			$message = 'The given export dir is not writeable at {dir}';
			$context = ['{dir}' => $exportDir];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		// the file is ~80mb, do not put it in memory
		$uri = $this->_uriFactory->createUri(self::HOST.'exploitation/reuse/export');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$request = $request->withAddedHeader('X-Php-Download-File', $path);
		$response = $this->_httpClient->sendRequest($request);
		
		$header = $response->getHeaderLine('X-Php-Uncompressed-File');
		if(!empty($header))
		{
			if(!@\rename($header, $path))
			{
				$message = 'Failed to rename downloaded file at "{header}" to "{path}"';
				$context = ['{header}' => $header, '{path}' => $path];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		return $response->getStatusCode() === 200;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrDatatourismeProducteur\ApiFrDatatourismeProducteurEndpointInterface::getReutilisationStatsFromFile()
	 */
	public function getReutilisationStatsFromFile(string $path) : Iterator
	{
		// if date < 2018-05-17
		// producteur/diffuseur/type/codePostal/modeTelechargement/application/url/dateTelechargement/typePrincipal/TypeSecondaire/createur/total/
		// if date >= 2018-05-17
		// producteur/diffuseur/type/codePostal/pays/modeTelechargement/application/url/dateTelechargement/typePrincipal/TypeSecondaire/createur/total
		//                                      ^^^^
		$iterator = new CsvFileDataIterator($path, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->_reifier->reifyIterator(ApiFrDatatourismeProducteurStatReutilisationLine::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrDatatourismeProducteur\ApiFrDatatourismeProducteurEndpointInterface::getTodaysReutilisationStats()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws LogicException
	 * @throws ParseThrowable should not happen
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	public function getTodaysReutilisationStats() : Iterator
	{
		$yearDir = $this->_storeDirectory.\DIRECTORY_SEPARATOR.\date('Y');
		
		if(!\is_dir($yearDir))
		{
			// @codeCoverageIgnoreStart
			if(!\mkdir($yearDir))
			{
				$message = 'Failed to create directory at {dir}';
				$context = ['{dir}' => $yearDir];
				
				throw new RuntimeException(\strtr($message, $context));
			}
			// @codeCoverageIgnoreEnd
		}
		
		if(!\is_writable($yearDir))
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to write in directory at {dir}';
			$context = ['{dir}' => $yearDir];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$filePath = $yearDir.\DIRECTORY_SEPARATOR.'reutilisation-'.\date('Y-m-d').'.csv';
		
		$this->copyTodaysReutilisationStatsFile($filePath);
		
		return $this->getReutilisationStatsFromFile($filePath);
	}
	
	/**
	 * Gets the slugifier from the factory if needed.
	 * 
	 * @return SlugifierInterface
	 */
	protected function getSlugifier() : SlugifierInterface
	{
		if(null === $this->_slugifier)
		{
			$this->_slugifier = $this->_slugifierFactory->createSlugifier();
		}
		
		return $this->_slugifier;
	}
	
}
