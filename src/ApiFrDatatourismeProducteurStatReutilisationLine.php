<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-producteur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeProducteur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeProducteurStatReutilisationLine class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeProducteurStatReutilisationLineInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
class ApiFrDatatourismeProducteurStatReutilisationLine implements ApiFrDatatourismeProducteurStatReutilisationLineInterface
{
	
	/**
	 * The name of the producer of the flux.
	 * 
	 * @var string
	 */
	protected string $_producteur;
	
	/**
	 * The name of the diffuseur of the flux.
	 * 
	 * @var string
	 */
	protected string $_diffuseur;
	
	/**
	 * The name of the type of diffuseur.
	 * 
	 * @var string
	 */
	protected string $_type;
	
	/**
	 * The code postal of the diffuseur.
	 * 
	 * @var ?string
	 */
	protected ?string $_codePostal = null;
	
	/**
	 * The country of the diffuseur.
	 * 
	 * @var ?string
	 */
	protected ?string $_pays = null;
	
	/**
	 * The name of the mode of download.
	 * 
	 * @var string
	 */
	protected string $_modeTelechargement;
	
	/**
	 * The name of the diffuseur's application that uses the flux.
	 * 
	 * @var string
	 */
	protected string $_application;
	
	/**
	 * The url of the diffuseur's application.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_url = null;
	
	/**
	 * When the flux was used.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateTelechargement;
	
	/**
	 * The name of the primary type of point of interest in the flux.
	 * 
	 * @var string
	 */
	protected string $_typePrincipal;
	
	/**
	 * The name of the secondary type of point of interest in the flux.
	 * 
	 * @var ?string
	 */
	protected ?string $_typeSecondaire = null;
	
	/**
	 * The creator of the data from the producer.
	 * 
	 * @var string
	 */
	protected string $_createur;
	
	/**
	 * The total number of points of interest gathered in the flux that day.
	 * 
	 * @var int
	 */
	protected int $_total;
	
	/**
	 * Constructor for ApiFrDatatourismeProducteurStatReutilisationLine with private members.
	 * 
	 * @param string $producteur
	 * @param string $diffuseur
	 * @param string $type
	 * @param string $modeTelechargement
	 * @param string $application
	 * @param DateTimeInterface $dateTelechargement
	 * @param string $typePrincipal
	 * @param string $createur
	 * @param int $total
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(string $producteur, string $diffuseur, string $type, string $modeTelechargement, string $application, DateTimeInterface $dateTelechargement, string $typePrincipal, string $createur, int $total)
	{
		$this->setProducteur($producteur);
		$this->setDiffuseur($diffuseur);
		$this->setType($type);
		$this->setModeTelechargement($modeTelechargement);
		$this->setApplication($application);
		$this->setDateTelechargement($dateTelechargement);
		$this->setTypePrincipal($typePrincipal);
		$this->setCreateur($createur);
		$this->setTotal($total);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the name of the producer of the flux.
	 * 
	 * @param string $producteur
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setProducteur(string $producteur) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_producteur = $producteur;
		
		return $this;
	}
	
	/**
	 * Gets the name of the producer of the flux.
	 * 
	 * @return string
	 */
	public function getProducteur() : string
	{
		return $this->_producteur;
	}
	
	/**
	 * Sets the name of the diffuseur of the flux.
	 * 
	 * @param string $diffuseur
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setDiffuseur(string $diffuseur) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_diffuseur = $diffuseur;
		
		return $this;
	}
	
	/**
	 * Gets the name of the diffuseur of the flux.
	 * 
	 * @return string
	 */
	public function getDiffuseur() : string
	{
		return $this->_diffuseur;
	}
	
	/**
	 * Sets the name of the type of diffuseur.
	 * 
	 * @param string $type
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setType(string $type) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the name of the type of diffuseur.
	 * 
	 * @return string
	 */
	public function getType() : string
	{
		return $this->_type;
	}
	
	/**
	 * Sets the code postal of the diffuseur.
	 * 
	 * @param ?string $codePostal
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setCodePostal(?string $codePostal) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_codePostal = $codePostal;
		
		return $this;
	}
	
	/**
	 * Gets the code postal of the diffuseur.
	 * 
	 * @return ?string
	 */
	public function getCodePostal() : ?string
	{
		return $this->_codePostal;
	}
	
	/**
	 * Sets the country of the diffuseur.
	 * 
	 * @param ?string $pays
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setPays(?string $pays) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_pays = $pays;
		
		return $this;
	}
	
	/**
	 * Gets the country of the diffuseur.
	 * 
	 * @return ?string
	 */
	public function getPays() : ?string
	{
		return $this->_pays;
	}
	
	/**
	 * Sets the name of the mode of download.
	 * 
	 * @param string $modeTelechargement
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setModeTelechargement(string $modeTelechargement) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_modeTelechargement = $modeTelechargement;
		
		return $this;
	}
	
	/**
	 * Gets the name of the mode of download.
	 * 
	 * @return string
	 */
	public function getModeTelechargement() : string
	{
		return $this->_modeTelechargement;
	}
	
	/**
	 * Sets the name of the diffuseur's application that uses the flux.
	 * 
	 * @param string $application
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setApplication(string $application) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_application = $application;
		
		return $this;
	}
	
	/**
	 * Gets the name of the diffuseur's application that uses the flux.
	 * 
	 * @return string
	 */
	public function getApplication() : string
	{
		return $this->_application;
	}
	
	/**
	 * Sets the url of the diffuseur's application.
	 * 
	 * @param ?UriInterface $url
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setUrl(?UriInterface $url) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_url = $url;
		
		return $this;
	}
	
	/**
	 * Gets the url of the diffuseur's application.
	 * 
	 * @return ?UriInterface
	 */
	public function getUrl() : ?UriInterface
	{
		return $this->_url;
	}
	
	/**
	 * Sets when the flux was used.
	 * 
	 * @param DateTimeInterface $dateTelechargement
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setDateTelechargement(DateTimeInterface $dateTelechargement) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_dateTelechargement = $dateTelechargement;
		
		return $this;
	}
	
	/**
	 * Gets when the flux was used.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateTelechargement() : DateTimeInterface
	{
		return $this->_dateTelechargement;
	}
	
	/**
	 * Sets the name of the primary type of point of interest in the flux.
	 * 
	 * @param string $typePrincipal
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setTypePrincipal(string $typePrincipal) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_typePrincipal = $typePrincipal;
		
		return $this;
	}
	
	/**
	 * Gets the name of the primary type of point of interest in the flux.
	 * 
	 * @return string
	 */
	public function getTypePrincipal() : string
	{
		return $this->_typePrincipal;
	}
	
	/**
	 * Sets the name of the secondary type of point of interest in the flux.
	 * 
	 * @param ?string $typeSecondaire
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setTypeSecondaire(?string $typeSecondaire) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_typeSecondaire = $typeSecondaire;
		
		return $this;
	}
	
	/**
	 * Gets the name of the secondary type of point of interest in the flux.
	 * 
	 * @return ?string
	 */
	public function getTypeSecondaire() : ?string
	{
		return $this->_typeSecondaire;
	}
	
	/**
	 * Sets the creator of the data from the producer.
	 * 
	 * @param string $createur
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setCreateur(string $createur) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_createur = $createur;
		
		return $this;
	}
	
	/**
	 * Gets the creator of the data from the producer.
	 * 
	 * @return string
	 */
	public function getCreateur() : string
	{
		return $this->_createur;
	}
	
	/**
	 * Sets the total number of points of interest gathered in the flux that
	 * day.
	 * 
	 * @param int $total
	 * @return ApiFrDatatourismeProducteurStatReutilisationLineInterface
	 */
	public function setTotal(int $total) : ApiFrDatatourismeProducteurStatReutilisationLineInterface
	{
		$this->_total = $total;
		
		return $this;
	}
	
	/**
	 * Gets the total number of points of interest gathered in the flux that
	 * day.
	 * 
	 * @return int
	 */
	public function getTotal() : int
	{
		return $this->_total;
	}
	
}
