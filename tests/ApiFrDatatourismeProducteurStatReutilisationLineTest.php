<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-producteur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeProducteur\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrDatatourismeProducteur\ApiFrDatatourismeProducteurStatReutilisationLine;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeProducteurStatReutilisationLineTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeProducteur\ApiFrDatatourismeProducteurStatReutilisationLine
 * @internal
 * @small
 */
class ApiFrDatatourismeProducteurStatReutilisationLineTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeProducteurStatReutilisationLine
	 */
	protected ApiFrDatatourismeProducteurStatReutilisationLine $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetProducteur() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getProducteur());
		$expected = 'qsdfghjklm';
		$this->_object->setProducteur($expected);
		$this->assertEquals($expected, $this->_object->getProducteur());
	}
	
	public function testGetDiffuseur() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getDiffuseur());
		$expected = 'qsdfghjklm';
		$this->_object->setDiffuseur($expected);
		$this->assertEquals($expected, $this->_object->getDiffuseur());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getType());
		$expected = 'qsdfghjklm';
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetCodePostal() : void
	{
		$this->assertNull($this->_object->getCodePostal());
		$expected = 'qsdfghjklm';
		$this->_object->setCodePostal($expected);
		$this->assertEquals($expected, $this->_object->getCodePostal());
	}
	
	public function testGetPays() : void
	{
		$this->assertNull($this->_object->getPays());
		$expected = 'qsdfghjklm';
		$this->_object->setPays($expected);
		$this->assertEquals($expected, $this->_object->getPays());
	}
	
	public function testGetModeTelechargement() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getModeTelechargement());
		$expected = 'qsdfghjklm';
		$this->_object->setModeTelechargement($expected);
		$this->assertEquals($expected, $this->_object->getModeTelechargement());
	}
	
	public function testGetApplication() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getApplication());
		$expected = 'qsdfghjklm';
		$this->_object->setApplication($expected);
		$this->assertEquals($expected, $this->_object->getApplication());
	}
	
	public function testGetUrl() : void
	{
		$this->assertNull($this->_object->getUrl());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setUrl($expected);
		$this->assertEquals($expected, $this->_object->getUrl());
	}
	
	public function testGetDateTelechargement() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), $this->_object->getDateTelechargement());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setDateTelechargement($expected);
		$this->assertEquals($expected, $this->_object->getDateTelechargement());
	}
	
	public function testGetTypePrincipal() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getTypePrincipal());
		$expected = 'qsdfghjklm';
		$this->_object->setTypePrincipal($expected);
		$this->assertEquals($expected, $this->_object->getTypePrincipal());
	}
	
	public function testGetTypeSecondaire() : void
	{
		$this->assertNull($this->_object->getTypeSecondaire());
		$expected = 'qsdfghjklm';
		$this->_object->setTypeSecondaire($expected);
		$this->assertEquals($expected, $this->_object->getTypeSecondaire());
	}
	
	public function testGetCreateur() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCreateur());
		$expected = 'qsdfghjklm';
		$this->_object->setCreateur($expected);
		$this->assertEquals($expected, $this->_object->getCreateur());
	}
	
	public function testGetTotal() : void
	{
		$this->assertEquals(12, $this->_object->getTotal());
		$expected = 25;
		$this->_object->setTotal($expected);
		$this->assertEquals($expected, $this->_object->getTotal());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeProducteurStatReutilisationLine('azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), 'azertyuiop', 'azertyuiop', 12);
	}
	
}
