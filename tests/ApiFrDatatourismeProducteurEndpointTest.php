<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-producteur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrDatatourismeProducteur\ApiFrDatatourismeProducteurEndpoint;
use PhpExtended\ApiFrDatatourismeProducteur\ApiFrDatatourismeProducteurStatReutilisationLine;
use PhpExtended\HttpMessage\FileStream;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiFrDatatourismeProducteurEndpointTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeProducteur\ApiFrDatatourismeProducteurEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiFrDatatourismeProducteurEndpointTest extends TestCase
{
	
	/**
	 * Gets a suitable directory to download and uncompress data.
	 *
	 * @return string
	 * @throws RuntimeException
	 */
	protected static function getTempPath() : string
	{
		$base = \is_dir('/media/anastaszor/RUNTIME/') ? '/media/anastaszor/RUNTIME/' : '/tmp/';
		$real = $base.'php-extended__php-api-fr-gouv-datatourisme-producteur-object';
		if(!\is_dir($real))
		{
			if(!\mkdir($real))
			{
				throw new RuntimeException('Failed to make temp directory at '.$real);
			}
		}
		
		return $real;
	}
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeProducteurEndpoint
	 */
	protected ApiFrDatatourismeProducteurEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testCopyTodaysReutilisationStatsFile() : void
	{
		if(\is_file(__DIR__.'/../credentials.cache'))
		{
			$credentials = require __DIR__.'/../credentials.cache';
			$this->_object->login($credentials['username'], $credentials['password']);
		}
		else
		{
			$this->markTestSkipped('No credentials provided.');
			
			return;
		}
		
		$this->assertTrue($this->_object->copyTodaysReutilisationStatsFile('__file__'));
	}
	
	public function testGetTodayReutilisationLines() : void
	{
		if(\is_file(__DIR__.'/../credentials.cache'))
		{
			$credentials = require __DIR__.'/../credentials.cache';
			$this->_object->login($credentials['username'], $credentials['password']);
		}
		else
		{
			$this->markTestSkipped('No credentials provided.');
			
			return;
		}
		
		$k = 0;
		
		foreach($this->_object->getTodaysReutilisationStats() as $line)
		{
			$this->assertInstanceOf(ApiFrDatatourismeProducteurStatReutilisationLine::class, $line);
			$k++;
		}
		
		$this->assertGreaterThan(1, $k);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			protected ?string $_cookies = null;
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$response = new Response();
				$response = $response->withHeader('X-Request-Uri', $request->getUri()->__toString());
				
				foreach($request->getHeaders() as $name => $value)
				{
					$response = $response->withAddedHeader('X-Request-Header-'.$name, $value);
				}
				
				$headers = [];
				
				$filePath = $request->getHeaderLine('X-Php-Download-File');
				$request = $request->withoutHeader('X-Php-Download-File');
				$request = $request->withoutHeader('X-Php-Follow-Location');
				
				foreach($request->getHeaders() as $headerKey => $headerArr)
				{
					$headers[] = $headerKey.': '.\implode(', ', $headerArr);
				}
				
				if(!empty($this->_cookies))
				{
					$headers[] = 'Cookie: '.$this->_cookies;
				}
				
				$httpData = [
					'method' => $request->getMethod(),
					'header' => \implode("\r\n", $headers)."\r\n",
					'content' => $request->getBody()->__toString(),
					'follow_location' => 0,
					'timeout' => 600.0,
				];
				
				// \var_dump($request->getUri()->__toString(), $httpData);
				
				$context = \stream_context_create(['http' => $httpData]);
				
				$http_response_header = [];
				
				if(!empty($filePath))
				{
					\copy($request->getUri()->__toString(), $filePath, $context);
					$stream = new FileStream($filePath);
				}
				else
				{
					$data = (string) \file_get_contents($request->getUri()->__toString(), false, $context);
					$stream = new StringStream($data);
				}
				
				// \var_dump($http_response_header);
				
				foreach($http_response_header as $header)
				{
					if(\mb_strpos($header, 'HTTP/') !== false)
					{
						continue;
					}
					$dotpos = \mb_strpos($header, ':');
					$name = \mb_substr($header, 0, $dotpos);
					$value = \trim(\mb_substr($header, $dotpos + 1));
					$response = $response->withAddedHeader($name, $value);
					if('Set-Cookie' === $name)
					{
						$this->_cookies = \mb_substr($value, 0, 59);
					}
				}
				
				return $response->withBody($stream);
			}
			
		};
		
		$this->_object = new ApiFrDatatourismeProducteurEndpoint($this->getTempPath(), $client);
	}
	
}
