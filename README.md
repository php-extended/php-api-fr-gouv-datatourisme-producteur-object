# php-extended/php-api-fr-gouv-datatourisme-producteur-object

A library that implements the php-extended/php-api-fr-gouv-datatourisme-producteur-interface library.

![coverage](https://gitlab.com/php-extended/php-api-fr-gouv-datatourisme-producteur-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-gouv-datatourisme-producteur-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-gouv-datatourisme-producteur-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\DatatourismeProducteurApi\DtProdApiEndpoint;
use PhpExtended\Endpoint\HttpEndpoint;

/** @var $client \Psr\Http\Client\ClientInterface */
// note : the $client must support cookies, should not follow
// locations, and must have extended timeout

$endpoint = new DtProdApiEndpoint('/tmp/', new HttpClient($client));

$endpoint->login($username, $password);

foreach($endpoint->getTodaysReutilisationStats() as $line)
{
	// $line instanceof DtProdApiStatReutilisationLine
	// do something with $line
}

```


## License

MIT (See [license file](LICENSE)).
